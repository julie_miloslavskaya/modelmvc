<?php

class View
{
    public $canEdit;
    public $pagination;

    function __construct()
    {
    }

    function generate($content_view, $template_view, $data = null)
    {
        include 'application/views/' . $template_view;
    }

    /**
     * @param bool $canEdit
     */
    public function setCanEdit($canEdit)
    {
        $this->canEdit = $canEdit;
    }

    /**
     * @return mixed
     */
    public function getCanEdit()
    {
        return $this->canEdit;
    }

    /**
     * @param mixed $pagination
     */
    public function setPagination($pagination)
    {
        $this->pagination = $pagination;
    }

    /**
     * @return mixed
     */
    public function getPagination()
    {
        return $this->pagination;
    }
}