<?php

class Controller
{
    const ADMIN_LOGIN = 'admin';
    const ADMIN_PASWWD = '123';
    public $model;
    public $view;
    public $adminLoggedIn;
    public $sort;
    public $order;
    public $pagination;

    function __construct()
    {
        $this->adminLoggedIn = isset($_SESSION[self::ADMIN_LOGIN]) && $_SESSION[self::ADMIN_LOGIN] == self::ADMIN_PASWWD;
        $this->view = new View();
        $this->view->setCanEdit($this->adminLoggedIn);
        $this->sort = 'id';
        $this->order = 'desc';
        if (!empty($_GET['sort']) && in_array($_GET['sort'], ['name', 'email', 'status']))
            $this->sort = mb_strtolower(trim($_GET['sort']));
        if (!empty($_GET['order']) && in_array($_GET['order'], ['asc', 'desc']))
            $this->order = mb_strtolower(trim($_GET['order']));
    }
}