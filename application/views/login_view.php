<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h1>Страница авторизации</h1>
            <form action="" method="post">
                <div class="form-group">
                    <label for="login">Логин</label>
                    <input type="text" class="form-control" id="login" name="login" required maxlength="100">
                </div>
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input type="password" class="form-control" id="password" name="password" required maxlength="100">
                </div>
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </form>
            <?php extract($data); ?>
            <?php if ($login_status == "access_granted") { ?>
                <div class="alert alert-success" role="alert">
                    Авторизация прошла успешно.
                </div>
            <?php } elseif ($login_status == "access_denied") { ?>
                <div class="alert alert-danger" role="alert">
                    Логин и/или пароль введены неверно.
                </div>
            <?php } ?>
        </div>
    </div>
</div>
