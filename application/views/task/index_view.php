<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h1>Список задач</h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Имя пользователя</th>
                    <th scope="col">Email</th>
                    <th scope="col">Текст задачи</th>
                    <th scope="col">Статус</th>
                    <?php if ($this->canEdit) { ?>
                        <th scope="col">Действия администратора</th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>

                <?php
                if (count($data)) {
                    foreach ($data as $row) {
                        ?>
                        <tr>
                            <th scope="row"><?php echo $row['id']; ?></th>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['email']; ?></td>
                            <td><?php echo $row['text']; ?></td>
                            <td><?php echo ((int)$row['status']) ? 'выполнено' : ''; ?>
                                <?php echo ((int)$row['edited']) ? '<br>отредактировано' : ''; ?>
                            </td>
                            <?php if ($this->canEdit) { ?>
                                <td>
                                    <a class="btn btn-light" href="/task/edit/<?php echo $row['id']; ?>" role="button">Редактировать</a>

                                </td>
                            <?php } ?>
                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
            <?php if ($this->pagination['pages'] > 1) { ?>
                <nav aria-label="Пагинация">
                    <ul class="pagination justify-content-center">
                        <li class="page-item <?php if ($this->pagination['currentPage'] == 1) { ?>disabled<?php } ?>">
                            <a class="page-link"
                               href="<?php if ($this->pagination['currentPage'] > 1) {
                                   $this->pagination['query']['page'] = $this->pagination['currentPage'] - 1;
                                   echo '/?' . http_build_query($this->pagination['query']);
                               } else {
                                   echo '#';
                               } ?>"
                               tabindex="-1"
                               <?php echo ($this->pagination['currentPage'] > 1) ? '' : ' aria-disabled="true"'; ?>>Назад</a>
                        </li>
                        <?php for ($i = 1; $i <= $this->pagination['pages']; $i++) { ?>
                            <li class="page-item <?php echo $this->pagination['currentPage'] == $i ? 'active' : ''; ?>" <?php echo $this->pagination['currentPage'] == $i ? 'aria-current="page"' : ''; ?>>
                                <a class="page-link"
                                   href="<?php $this->pagination['query']['page'] = $i;
                                   echo '/?' . http_build_query($this->pagination['query']); ?>"><?php echo $i; ?><?php echo $this->pagination['currentPage'] == $i ? '<span class="sr-only">(current)</span>' : ''; ?></a>
                            </li>
                        <?php } ?>
                        <li class="page-item <?php if ($this->pagination['currentPage'] == $this->pagination['pages']) { ?>disabled<?php } ?>">
                            <a class="page-link"
                               href="<?php if ($this->pagination['currentPage'] < $this->pagination['pages']) {
                                   $this->pagination['query']['page'] = $this->pagination['currentPage'] + 1;
                                   echo '/?' . http_build_query($this->pagination['query']);
                               } else {
                                   echo '#';
                               } ?>">Вперёд</a>
                        </li>

                    </ul>
                </nav>
            <?php } ?>
        </div>
        <div class="col-sm">
            <div class="row">
                <div class="col">
                    <?php if (!$this->canEdit) { ?>
                        <a class="btn btn-light" href="/login" role="button">Войти</a>
                    <?php } else { ?>
                        <a class="btn btn-light" href="/admin/logout" role="button">Выйти</a>
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <a class="btn btn-primary" href="/task/new" role="button">Создать новую задачу</a>
                    <div class="row">
                        <div class="col">
                            <h4>Сортировать список задач по</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <a class="btn btn-light" href="/?sort=name&order=asc" role="button">Возрастанию имени</a>
                            <a class="btn btn-light" href="/?sort=name&order=desc" role="button">Убыванию имени</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-light" href="/?sort=email&order=asc" role="button">Возрастанию Email</a>
                            <a class="btn btn-light" href="/?sort=email&order=desc" role="button">Убыванию Email</a>
                        </div>
                        <div class="col">
                            <a class="btn btn-light" href="/?sort=status&order=asc" role="button">Возрастанию
                                статуса</a>
                            <a class="btn btn-light" href="/?sort=status&order=desc" role="button">Убыванию статуса</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>