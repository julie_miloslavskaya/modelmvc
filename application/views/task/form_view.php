<div class="container-fluid">
    <div class="row">
        <div class="col">
            <h1>Задача</h1>
            <form action="" method="post">
                <?php if (!empty($data['id'])) { ?>
                    <input type="hidden" name="id" value="<?php echo $data['id']; ?>">
                <?php } ?>
                <?php if (isset($data['edited'])) { ?>
                    <input type="hidden" name="edited" value="<?php echo (int)$data['edited']; ?>">
                <?php } ?>
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input type="text" class="form-control" id="name" name="name" required maxlength="100"
                           value="<?php echo !empty($data['name']) ? $data['name'] : ''; ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" required maxlength="100"
                           value="<?php echo !empty($data['email']) ? $data['email'] : ''; ?>">
                </div>
                <div class="form-group">
                    <label for="text">Текст</label>
                    <textarea class="form-control" id="text" rows="5" name="text" required
                              maxlength="500"><?php echo !empty($data['text']) ? $data['text'] : ''; ?></textarea>
                </div>
                <?php if ($this->canEdit) { ?>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-control" id="status"
                               name="status" <?php echo !empty($data['status']) && (int)$data['status'] ? 'checked' : ''; ?>>
                        <label for="status">Выполнена ли задача?</label>
                    </div>
                <?php } ?>
                <button type="submit" class="btn btn-primary">Сохранить</button>
                <small>Все поля обязательные к заполнению</small>
            </form>
        </div>
        <div class="col">
            <a class="btn btn-primary" href="/" role="button">К списку задач</a>
        </div>
    </div>
    <?php if (isset($data['success']) && $data['success']) { ?>
        <div class="row">
            <div class="col">
                <div class="alert alert-success" role="alert">
                    Данные успешно сохранены!
                </div>
            </div>
        </div>
    <?php } ?>
</div>