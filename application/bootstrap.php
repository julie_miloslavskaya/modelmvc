<?php
setlocale(LC_ALL, 'ru_RU.utf8');
mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_language('uni');
header('Content-type: text/html; charset=utf-8');
date_default_timezone_set('Europe/Moscow');
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';
Route::start();