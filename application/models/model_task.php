<?php

class Model_Task extends Model
{
    private $fileName;

    public function __construct()
    {
        $this->fileName = dirname(__FILE__) . '/../../data/list.csv';
    }

    /**
     * @param null $param
     * @return array|mixed|void
     */
    public function get_data($param = null)
    {
        if (!file_exists($this->fileName)) return;

        $file = fopen($this->fileName, 'r');
        $data = [];
        while (($line = fgetcsv($file, 0, ';')) !== FALSE)
            $data[$line[0]] = ['id' => $line[0], 'name' => $line[1], 'email' => $line[2],
                'text' => $line[3], 'status' => $line[4], 'edited' => $line[5]];
        fclose($file);

        if (!is_null($param) && is_int($param) && isset($data[$param]))
            return $data[$param];

        return $data;
    }

    /**
     * @param $data
     * @param string $sort
     * @param string $order
     * @return mixed
     */
    public function sort_data($data, $sort = 'id', $order = 'desc')
    {
        $array_column = array_column($data, $sort);
        if ($order == 'desc')
            $order = SORT_DESC;
        else
            $order = SORT_ASC;
        array_multisort($array_column, $order, $data);
        return $data;
    }

    /**
     * @param array $data
     * @return bool|int
     */
    function save_data($data = [])
    {
        if (!count($data) || !isset($data['name']) || !isset($data['email']) || !isset($data['text'])
            || !isset($data['status']) || !isset($data['edited'])) return false;
        $lineCount = 0;
        if (file_exists($this->fileName)) {
            $file = fopen($this->fileName, "r");
            while (($line = fgetcsv($file, 0, ';')) !== FALSE) {
                $lineCount++;
            }
            fclose($file);
        }
        $fp = fopen($this->fileName, 'a+');
        $id = $lineCount + 1;
        fputcsv($fp, [$id, $data['name'], $data['email'], $data['text'], $data['status'], $data['edited']], ';');
        fclose($fp);
        return $id;
    }

    /**
     * @param $task
     * @return bool
     */
    function edit_data($task)
    {
        $data = $this->get_data();
        if (count($data) && isset($data[$task['id']]) && count($task))
            $data[$task['id']] = $task;
        else return false;
        $fp = fopen($this->fileName, 'w+');
        foreach ($data as $item)
            fputcsv($fp, [$item['id'], $item['name'], $item['email'], $item['text'],
                $item['status'], $item['edited']], ';');
        fclose($fp);
        return true;
    }

    /**
     * @param $post
     * @param $data
     * @return array
     */
    function filter_data($post, $data = [])
    {
        $result = [];
        foreach ($post as $key => $value) {
            $result[$key] = htmlspecialchars((stripslashes(trim($value))));
        }
        $result['status'] = (isset($result['status']) && $result['status'] == 'on' ? 1 : 0);
        if (count($data) && $post['text'] != $data['text'])
            $result['edited'] = 1;
        else
            $result['edited'] = 0;
        return $result;
    }
}