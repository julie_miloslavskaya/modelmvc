<?php

class Controller_Task extends Controller
{

    function __construct()
    {
        parent::__construct();
        $this->model = new Model_Task();
    }

    function action_new()
    {
        $data = [];
        if (count($_POST)) {
            $post = $_POST;
            $data = $this->model->filter_data($post);
            $id = $this->model->save_data($data);
            if ($id) {
                $data['success'] = true;
            }
        }
        $this->view->generate('task/form_view.php', 'template_view.php', $data);

    }

    function action_edit($param = null)
    {
        if (!$this->adminLoggedIn) {
            header('Location: /login/');
            exit();
        }
        if (is_null($param) || !is_int($param))
            Route::ErrorPage404();
        $data = $this->model->get_data($param);
        if (!count($data))
            Route::ErrorPage404();
        if (count($_POST)) {
            $post = $_POST;
            $dataEdited = $this->model->filter_data($post, $data);
            if ($this->model->edit_data($dataEdited)) {
                $data = $dataEdited;
                $data['success'] = true;
            }
        }
        $this->view->generate('task/form_view.php', 'template_view.php', $data);
    }


}