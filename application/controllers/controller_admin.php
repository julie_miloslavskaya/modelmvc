<?php

class Controller_Admin extends Controller
{

    function action_index()
    {
        if ($this->adminLoggedIn) {
            $this->view->generate('admin_view.php', 'template_view.php');
        } else {
            session_destroy();
            Route::ErrorPage404();
        }

    }

    function action_logout()
    {
        session_destroy();
        header('Location:/');
    }

}