<?php
include "application/models/model_task.php";

class Controller_Main extends Controller
{
    /**
     * Количество задач на странице.
     */
    const PAGE_SIZE = 3;

    function __construct()
    {
        parent::__construct();
        $this->model = new Model_Task();
    }

    function action_index()
    {
        $array = $this->model->get_data();
        $this->pagination['currentPage'] = 1;
        $this->pagination['pages'] = 0;
        if (!empty($_GET['page']))
            $this->pagination['currentPage'] = (int)$_GET['page'];
        if ($array) {
            $data = $this->model->sort_data($array, $this->sort, $this->order);
            $count = count($data);
            $this->pagination['pages'] = ceil($count / self::PAGE_SIZE);
        }
        $query = [];
        if ($this->pagination['currentPage'] > 1)
            $query['page'] = $this->pagination['currentPage'];
        if ($this->sort != 'id')
            $query['sort'] = $this->sort;
        if (!empty($this->order))
            $query['order'] = $this->order;

        $this->pagination['query'] = $query;
        $this->view->setPagination($this->pagination);
        $result = array_slice($data, ($this->pagination['currentPage'] - 1) * self::PAGE_SIZE, self::PAGE_SIZE);
        $this->view->generate('task/index_view.php', 'template_view.php', $result);
    }
}